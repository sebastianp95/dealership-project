package dev.perez.services;

import dev.perez.data.UserDao;
import dev.perez.data.UserDaoHibImpl;
import dev.perez.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserService {
    private Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserDao userDao = new UserDaoHibImpl();
    public User authenticate(String username , String password){
        return userDao.authenticateUser(username, password);
    }

    public User loadUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

}
